import {Button} from "../control/Button";
import {$$, by, element} from "protractor";
import {TextField} from "../control/TextField";
import {Bottom} from "./Bottom";


export class Header {

  constructor(){

  };

  private mpsIcon = new Button($$(".mps-icon-mps"));
  private searchField = new TextField(by.model("search.searchTerm"));
  private calendarIcon = new Button($$(".icon-calendar"));
  private bottom: Bottom = new Bottom();

  public async openMainPageByClickMpsIcon(){
    await this.mpsIcon.click();
  };

  public async chooseYear(year: string){
    let selectedYear = element($$('div[label=' + year + '] input'));
    await this.calendarIcon.click();
    await selectedYear.click();
    await this.bottom.save();
  }

}

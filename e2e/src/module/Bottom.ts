import {Button} from "../control/Button";
import {by} from "protractor";


export class Bottom{

  constructor(){

  };

  private saveBtn = new Button(by.buttonText("Speichern"));
  private cancelBtn = new Button(by.buttonText("Verwerfen"));

  public async save(){
    await this.saveBtn.click();
  };

  public async cancel(){
    await this.cancelBtn.click();
  };

  }

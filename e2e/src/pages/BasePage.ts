import {browser} from "protractor";
import {Bottom} from "../module/Bottom";


export class BasePage{

  constructor(){

  };

 protected bottom: Bottom = new Bottom();

 public async refresh(){
   await browser.refresh();
 };
}

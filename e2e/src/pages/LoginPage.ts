import {TextField} from "../control/TextField";
import {by} from "protractor";
import {Button} from "../control/Button";
import LoginData from "../data/loginData.json";
import {BasePage} from "./BasePage";
import {HomePage} from "./HomePage";


export class LoginPage extends BasePage{

  constructor(){
    super();
  }

  private loginField = new TextField(by.model("auth.loginData.userName"));
  private passwordField = new TextField(by.model("auth.loginData.password"));
  private loginBtn = new Button(by.css("btnLogin"));

  private userName: string = LoginData.admin.user;
  private password: string = LoginData.admin.password;


  public async loginAsAdmin(){
    await this.loginField.setValue(this.userName);
    await this.passwordField.setValue(this.password);
    return await new HomePage();
  }
}

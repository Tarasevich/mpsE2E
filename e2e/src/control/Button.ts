import {BaseControl} from "./BaseControl";
import {Locator} from "protractor";


export class Button extends BaseControl {

  constructor(locator: Locator){
    super(locator);
  };

  public async click(){
    await this.element.click();
  };

  
}

import {browser, element, ElementFinder, Locator} from "protractor";


export class BaseControl {

  protected element: ElementFinder;

  constructor(locator: Locator) {
    this.element = element(locator);
  }

  public async isDisplayed() {
    return await this.element.isDisplayed();
  };

  public async isElementPresent(){
    return await browser.isElementPresent(this.element);
  };

  public async getText(){
    return await this.element.getText();
  };

  public async getValue(){
    return await this.element.getAttribute("value");
  };

}
